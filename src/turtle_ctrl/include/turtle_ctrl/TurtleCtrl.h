/*! \file
 *
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 15.03.2021
 *
 * \copyright Copyright 2021 Chalmers
 *
 * #### License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#ifndef TURTLE_CTRL
#define TURTLE_CTRL

/*! \file TurtleCtrl.h
 *   \brief Controls the turtle to move from the current state to the desired state
 *
 *   Provides the following functionalities:
 *     - Subscriber to a TurtleStateStamped topic (desired state)
 *     - Publisher of a TurtleStateStamped topic (for the turtle visualizer)
 *     - TF frames
 */

// STD Library
#include <iostream>
#include <fstream>
#include <pthread.h>

// Eigen Library
#include <Eigen/Dense>
#include <Eigen/StdVector>

// ROS library
#include <ros/ros.h>

// tf publisher
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/transform_datatypes.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

// ROS custom messages
#include <turtle_msgs/TurtleStateStamped.h>

using namespace Eigen;

namespace turtle_space
{
class TurtleCtrl
{
private:
  /* data */

  ros::NodeHandle nh_;

  ros::Subscriber sub_cmd_pose_;     ///< Subscriber commanded turtle pose
  ros::Publisher pub_current_pose_;  ///< Publisher current turtle pose
  ros::Timer timer_;                 ///< Timer to run a parallel process

  tf2_ros::TransformBroadcaster tf_broadcaster_;  ///< Publisher for TF (to display commanded pose)

  turtle_msgs::TurtleStateStamped::ConstPtr turtle_cmd_msg_;  ///< Shared object to share the turtle state between the
                                                              ///< control loop and the subscriber

  std::string frame_id_;             ///< frame id for the commanded pose
  std::string subs_topic_name_;      ///< topic name for the subscriber (commanded pose)
  std::string pub_topic_name_;       ///< topic name for the turtle state publisher (for visualization)
  Eigen::Matrix3d K_;                ///< Control gain matrix
  Eigen::Vector3d turtle_current_;   ///< Current turtle position vector 3X1
  std::vector<double> turtle_init_;  ///< Initial position of the turtle
  int ctrl_period_;                  ///< Control period in ms

  std::mutex data_mutex_;  ///< Mutex to protect the reading/writing process

  bool first_message_;  ///< Flag to control when we have the turtle state
  bool init_ctrl_;      ///< Flag to set the turtle initial position using the TurtleState

public:
  /**
   * @brief Default constructor
   *
   */
  TurtleCtrl(/* args */);

  /**
   * @brief Destroy the Turtle Ctrl object
   *
   */
  ~TurtleCtrl();

  /**
   * @brief Control initialization, sets the control and node parameters
   *
   */
  void init();

private:
  /**
   * @brief Callback function to receive the TurtleState topic
   *
   * @param msg message with the current Turtle state (pose and velocity)
   */
  void topic_callback(const turtle_msgs::TurtleStateStamped::ConstPtr& msg);

  /**
   * @brief Callback function for the control loop. Internal thread for the controller.
   *
   */
  void timer_callback(const ros::TimerEvent& e);
};

}  // namespace turtle_space

#endif  // TURTLE_CTRL
