/*! \file
 *
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 15.03.2021
 *
 * \copyright Copyright 2021 Chalmers
 *
 * #### License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

// Turtle control class
#include <turtle_ctrl/TurtleCtrl.h>

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "turtle_control", ros::init_options::AnonymousName);

  ROS_INFO_STREAM("**Publishing turtle control..");

  ros::NodeHandle n;
  ros::Rate r(100);

  turtle_space::TurtleCtrl turtle_control;

  ros::spin();

  // If you need another process running in parallel, you can enable this loop
  // while (ros::ok())
  // {
  //   ros::spinOnce();
  //   ROS_INFO_STREAM("In loop");

  //   r.sleep();
  // }

  return 0;
}
