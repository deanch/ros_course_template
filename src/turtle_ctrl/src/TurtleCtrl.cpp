#include <turtle_ctrl/TurtleCtrl.h>

namespace turtle_space
{
TurtleCtrl::TurtleCtrl() : K_(Eigen::Matrix3d::Zero()), first_message_(false), init_ctrl_(true)
{
  // Init Parameters. Load node parameters
  init();

  // Create subscriber to receive the commanded turtle state. This state will be generated from a trajectory generator
  sub_cmd_pose_ = nh_.subscribe(subs_topic_name_, 100, &TurtleCtrl::topic_callback, this);

  // Create a publisher to update the current turtle state. This state will be used by the visualizer
  pub_current_pose_ = nh_.advertise<turtle_msgs::TurtleStateStamped>(pub_topic_name_, 10);

  // MAIN control thread. Using the commanded turtle state, the controller will move the turtle and
  // publish the current position
  timer_ = nh_.createTimer(ros::Duration(0.1), &TurtleCtrl::timer_callback, this);
}

TurtleCtrl::~TurtleCtrl()
{
  std::cout << "Dumb distructor" << std::endl;
}

void TurtleCtrl::topic_callback(const turtle_msgs::TurtleStateStamped::ConstPtr& msg)
{
  // The variable turtle_cmd_msg_ will be shared between the subscriber thread and the main control thread
  // We need to protect the reading/writing process using mutex
  data_mutex_.lock();
  turtle_cmd_msg_ = msg;
  data_mutex_.unlock();

  // TF message to visualize the commanded Turtle State
  std::vector<geometry_msgs::TransformStamped> v_ts;
  geometry_msgs::TransformStamped ts;

  // TF to populate the TF message
  tf2::Transform tf;

  // Auxiliary quaternion to define the orientation of the TF
  tf2::Quaternion qtf;

  // Get pose from msg
  // Convert a basic rotation in z to quaterion
  qtf.setRPY(0, 0, msg->pose.theta);
  // Set the position of the TF
  tf.setOrigin(tf2::Vector3(msg->pose.x, msg->pose.y, 0));
  // Set the orientation of the TF
  tf.setRotation(qtf);

  // Transform the TF to TF message
  ts.transform = tf2::toMsg(tf);
  // Set the reference frame
  ts.header.frame_id = "/world";
  // Set the time stamp for the message
  ts.header.stamp = ros::Time::now();
  // Define the name of the TF
  ts.child_frame_id = frame_id_;

  // Create ts msg
  v_ts.push_back(ts);

  // Publish the TF
  tf_broadcaster_.sendTransform(v_ts);

  // Flag to control when we have received a commanded turtle pose
  // This flag will activate the controller
  first_message_ = true;
}

void TurtleCtrl::timer_callback(const ros::TimerEvent& e)
{
  // When we have received a commanded Turtle state, we start the controller
  if (first_message_)
  {
    // turtle_cmd_msg_ is shared between two threads, therfore we need to protect it, using mutex.
    turtle_msgs::TurtleStateStamped::ConstPtr local_turtle_cmd;
    data_mutex_.lock();
    // We make a local copy of the shared variable. From this point on, we will work ONLY with the local copy,
    // and not with the shared variable. This is to avoid locking the other thread.
    local_turtle_cmd = turtle_cmd_msg_;
    data_mutex_.unlock();

    // Control
    // Vector for the commanded turtle pose
    Eigen::Vector3d turtle_cmd;
    // Pose error
    Eigen::Vector3d deltaX;

    // We initialize the controller internal state with the first turtle state message
    if (init_ctrl_)
    {
      // Set the pose vector with the pose from the message
      turtle_current_ << turtle_init_.at(0), turtle_init_.at(1), turtle_init_.at(2);
      // The pose initialization is done only the first time
      init_ctrl_ = false;
    }

    //  Set the commanded turtle pose using the turtle state message information
    turtle_cmd << local_turtle_cmd->pose.x, local_turtle_cmd->pose.y, local_turtle_cmd->pose.theta;

    // Calculate the error between the commanded pose and the current pose
    deltaX = turtle_cmd - turtle_current_;

    ROS_WARN_STREAM(ros::this_node::getName() << ": Pose Error " << deltaX.transpose());

    // Update the position of the turtle based on this error (simple Proportional control)
    turtle_current_ += K_ * deltaX;

    // Publish the current turtle state as a TurtleState message
    // This is important to debug the controller
    // Setting the time stamp for the message
    turtle_msgs::TurtleStateStamped turtle_cmd_msg;

    turtle_cmd_msg.header.stamp = ros::Time::now();
    // Setting the current pose
    turtle_cmd_msg.pose.x = turtle_current_(0);
    turtle_cmd_msg.pose.y = turtle_current_(1);
    turtle_cmd_msg.pose.theta = turtle_current_(2);

    // Publishing the current TurtleState message
    pub_current_pose_.publish(turtle_cmd_msg);
  }
}

void TurtleCtrl::init()
{
  // Manual definition of parameters
  //   marker_topic_name_ = "visualization_marker";
  //   marker_namespace_ = "turtle_marker";
  //   frame_id_ = "turtle_frame";
  //   subs_topic_name_ = "/turtle_pose";

  // Using the ros parameter server
  std::string param_name = "/turtle_control/frame_id";
  if (ros::param::has(param_name))
    ros::param::get(param_name, frame_id_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_control/subs_topic_name";
  if (ros::param::has(param_name))
    ros::param::get(param_name, subs_topic_name_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_control/pub_topic_name";
  if (ros::param::has(param_name))
    ros::param::get(param_name, pub_topic_name_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  std::vector<double> gains;
  param_name = "/turtle_control/gains";
  if (ros::param::has(param_name))
    ros::param::get(param_name, gains);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_control/ctrl_period";
  if (ros::param::has(param_name))
    ros::param::get(param_name, ctrl_period_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_control/turtle_init";
  if (ros::param::has(param_name))
    ros::param::get(param_name, turtle_init_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  // Populate the gain Matrix
  for (size_t i = 0; i < 3; i++)
  {
    K_(i, i) = gains.at(i);
  }

  ROS_INFO_STREAM("K:  \n" << K_);
}

}  // namespace turtle_space