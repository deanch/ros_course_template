#include <turtle_ctrl/TurtleVis.h>

namespace turtle_space
{
TurtleVis::TurtleVis(/* args */)
{
  // Init Parameters. Load node parameters
  init();

  // Marker publisher
  v_marker_publisher_ = nh_.advertise<visualization_msgs::MarkerArray>(marker_topic_name_, 1);

  subscription_ = nh_.subscribe(subs_topic_name_, 100, &TurtleVis::topic_callback, this);

  // Marker. Defines the Turtle CAD model and its relative transformation wrt to the reference frame
  turtle_marker_.header.frame_id = frame_id_;
  //   turtle_marker_.header.stamp = It must be defined before publishing the
  //   message;
  turtle_marker_.ns = marker_namespace_;
  turtle_marker_.id = 3452;
  turtle_marker_.type = visualization_msgs::Marker::MESH_RESOURCE;
  turtle_marker_.action = visualization_msgs::Marker::ADD;
  turtle_marker_.pose.position.x = 0.0;
  turtle_marker_.pose.position.y = 0.0;
  turtle_marker_.pose.position.z = 0.0;
  turtle_marker_.pose.orientation.x = 0;
  turtle_marker_.pose.orientation.y = 0;
  turtle_marker_.pose.orientation.z = 0.7071067811865475;
  turtle_marker_.pose.orientation.w = 0.7071067811865475;
  turtle_marker_.scale.x = 0.025;
  turtle_marker_.scale.y = 0.025;
  turtle_marker_.scale.z = 0.025;
  // This is the CAD model we want to visualize
  turtle_marker_.mesh_resource = "package://turtle_ctrl/urdf/meshes/turtle2.dae";
  //   This will force to use the original colors defined in the CAD model
  turtle_marker_.mesh_use_embedded_materials = false;

  turtle_marker_.color.r = 0.0;
  turtle_marker_.color.g = 0.7;
  turtle_marker_.color.b = 0.5;
  turtle_marker_.color.a = 1.0;
  turtle_marker_.lifetime = ros::Duration(0);
  turtle_marker_.frame_locked = true;
}

TurtleVis::~TurtleVis()
{
}

void TurtleVis::init()
{
  // Manual definition of parameters
  //   marker_topic_name_ = "visualization_marker";
  //   marker_namespace_ = "turtle_marker";
  //   frame_id_ = "turtle_frame";
  //   subs_topic_name_ = "/turtle_pose";

  // Using the ros parameter server
  std::string param_name = "/turtle_visualizer/frame_id";
  if (ros::param::has(param_name))
    ros::param::get(param_name, frame_id_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_visualizer/marker_namespace";
  if (ros::param::has(param_name))
    ros::param::get(param_name, marker_namespace_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_visualizer/marker_topic_name";
  if (ros::param::has(param_name))
    ros::param::get(param_name, marker_topic_name_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");

  param_name = "/turtle_visualizer/subs_topic_name";
  if (ros::param::has(param_name))
    ros::param::get(param_name, subs_topic_name_);
  else
    ROS_ERROR_STREAM("Param " << param_name << " not found");
}

void TurtleVis::topic_callback(const turtle_msgs::TurtleStateStamped::ConstPtr& msg)
{
  ROS_INFO_STREAM(ros::this_node::getName()
                  << ": Current Turtle Pos: " << msg->pose.x << ", " << msg->pose.y << ", " << msg->pose.theta);

  // Define the marker array container to publish as a message.
  // This is needed to visualize the CAD model in Rviz

  visualization_msgs::MarkerArray m_marker_msg;

  // Each marker is connected with a TF (frame_id)
  geometry_msgs::TransformStamped ts;

  // In this example, we will use a TF vector to show how to publish multiple TFs
  // However, we will only need one TF for the Marker
  std::vector<geometry_msgs::TransformStamped> v_ts;

  // Get the current time
  ros::Time aux_time = ros::Time::now();

  // TF object to populate our TF message
  tf2::Transform tf;

  //   tf.setOrigin(tf2::Vector3(0, 0, 0));

  // Auxiliary quaternion object to populate the TF message
  tf2::Quaternion qtf;

  // Populate the Marker Array to visualize the ATR as a mesh
  turtle_marker_.header.stamp = aux_time;

  // Get pose from msg
  // We define a quaternion using a basic rotation in z (Yaw)
  qtf.setRPY(0, 0, msg->pose.theta);

  // Set the position of the TF
  tf.setOrigin(tf2::Vector3(msg->pose.x, msg->pose.y, 0));

  // Set the orientation of the TF
  tf.setRotation(qtf);

  // Transform the TF object to TF message
  ts.transform = tf2::toMsg(tf);

  // Set the reference frame for the TF (parent link)
  ts.header.frame_id = "/world";
  // Set the time stamp for the message
  ts.header.stamp = aux_time;
  // Set the name for the TF
  ts.child_frame_id = frame_id_;

  //// To visualize objects in Rviz, we need to publish the marker (object) and its corresponding TF
  // Create TF msg
  v_ts.push_back(ts);
  // Publish transformations
  tf_broadcaster_.sendTransform(v_ts);

  // Create MarkerArray msg
  m_marker_msg.markers.push_back(turtle_marker_);
  // Publish Polygons as Line Markers
  v_marker_publisher_.publish(m_marker_msg);
}

}  // namespace turtle_space