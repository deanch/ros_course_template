import rospy

from turtle_trajectory_generator.trajectory_generator_module import Twists2CMD


def main(args=None):

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our node so that multiple nodes can
    # run simultaneously.
    rospy.init_node("listener", anonymous=True)

    twist2cmd_publisher = Twists2CMD()

    # spin() simply keeps python from exiting until this node is stopped

    rate = rospy.Rate(10)

    rospy.spin()


if __name__ == "__main__":
    main()
