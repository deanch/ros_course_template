#! /usr/bin/env python

import turtle_trajectory_generator.hello_module

from turtle_msgs.msg import *

if __name__ == "__main__":
    turtle_trajectory_generator.hello_module.say("my friend!")

    turtle_msg = TurtleStateStamped()

    turtle_trajectory_generator.hello_module.say(str(turtle_msg.pose.x))
