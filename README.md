# ROS Crash course

## Table of contents

1. [Problem Description](#problem-description)
1. [Installation](#install-necessary-packages)
2. [Task 1: Workspace](#task-1-create-your-new-workspace-overlay)
3. [Task 2: Get ros package](#task-2-get-teleop-ros-package-and-modify-it)
4. [Task 3: Ros message package](#task-3-create-a-new-ros-cpp-package-turtle-msgs)
5. [Task 4: Ros Control package](#task-4-create-a-new-ros-cpp-package-turtle-ctrl)
6. [Task 5: Launch Files](#task-5-let-us-create-a-lunch-file-to-automate-the-above-process)
7. [Task 6: Control](#task-6-create-the-controller)
8. [Task 7: Ros Trajectory Generator package](#task-7-trajectory-generator)
9. [Task 8: System Evaluation](#task-8-evaluate-full-system)
10. [Task 9: Recording and playing Bagfiles](#task-9-record-data-and-play-it-bagfiles)

---

## Problem description

[back](#table-of-contents)

We want to control the motion of a turtle

![Turtle1](/Resources/Figures/Problem/01_t.png "Turtle")

The turtle receives the a target pose (x,y,theta) with respect to a world coordinate frame. We need to generate the sequence of poses (control) to move the turtle from the current position to the target position.

![Turtle2](/Resources/Figures/Problem/02_t.png "Turtle control")

We also want to monitor the motion in a 3D visualization system.

![Turtle3](/Resources/Figures/Problem/03_t.png "Turtle vis")

We need to command the target pose of the turtle in two ways. First, using the keyboard. Second, with a trajectory generator that computes a smooth trajectory from the current position to the target position.

![Turtle4](/Resources/Figures/Problem/04_t.png "Turtle commands")

A solution to solve the problem is to divide it in five modules:

First, a module that receives continuously the current turtle position and displays it in the visualization system. The input of this module is a **TurtleState** message with the current turtle's position. The output is the visualization of the turtle.

![BD1](/Resources/Figures/Problem/01_bd.png "Visualization Module")

Then, a control module receives the commanded poses and computes the motions to reach them. The input of this module is the commanded turtle position (continuous or discrete). The output is the current position of the turtle (continuous). Both the input and output are of the same type **TurtleState**.

![BD2](/Resources/Figures/Problem/02_bd.png "Control Module")

We will use two methods to generate the commanded turtle position. One is based on a Spline generator which computes the sequences of turtle poses to reach the target position from the current position. The input is a service where a user can define the target pose and the time to reach it. The output is a continuous sequence of turtle poses of the type **TurtleState**.

![BD3](/Resources/Figures/Problem/03_bd.png "TG Spline Module")

We will also use keyboard commands. To this aim, we will use a standard ros package which gets the input of the keyboard and generates a discrete **Twist** velocity message. The keyboard commands will change the velocity of the turtle, both linear and angular.

![BD4](/Resources/Figures/Problem/04_bd.png "Keyboard Module")

Since the control module receives **TurtleState** messages as input, we need to transform the twist velocity message into continuous turtle commands (**TurtleState**). Therefore, we need to implement a module that transforms **Twist** velocities into **TurtleState** poses.

The user can switch between the Keyboard commands and the Spline trajectory.

![BD5](/Resources/Figures/Problem/05_bd.png "Twist2cmd Module")

Our goal is to use the ros middleware to implement these modules and solve the problem.

---

## Install necessary packages

[back](#table-of-contents)

For this course, you should have installed ros noetic in Ubuntu 20.04, and followed the beginner's tutorials (C++ and Python):

Ros installation:

<http://wiki.ros.org/noetic/Installation/Ubuntu>

Ros Tutorials:

<http://wiki.ros.org/ROS/Tutorials>

The following packages provide useful tools and you should also install them.

### Ubuntu and ROS packages

In a new terminal (CTRL+ALT+t)

    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list'
    wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt install git ssh ros-noetic-teleop-twist-keyboard terminator python3-catkin-tools

### Visual Studio Code

Install VSCode. Please follow the instructions "Installing Visual Studio Code with apt" from (<https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-20-04/>)

Install the following vscode extensions: (<https://code.visualstudio.com/docs/editor/extension-marketplace>)

- C/C++
- catkin-tools
- CMake
- Cmake Tools
- Pylance
- Python
- Python for VSCode
- Python preview
- ROS
- ROS snippets

---

## Task 1 Create your new workspace overlay

[back](#table-of-contents)

Open a terminal

    sudo rosdep init

Ignore the following error:

    ERROR: default sources list file already exists:
    	/etc/ros/rosdep/sources.list.d/20-default.list
    Please delete if you wish to re-initialize

This means that you have done this step before.

    rosdep update

    cd
    mkdir -p ~/ros/workspaces/ros_crash_ws/src
    source /opt/ros/noetic/setup.bash
    cd ~/ros/workspaces/ros_crash_ws/
    catkin_init_workspace

---

## Task 2 Get teleop ros package and modify it

[back](#table-of-contents)

### 2.1 Clone the ros package repo

    cd ~/ros/workspaces/ros_crash_ws/src/
    git clone git@github.com:ros-teleop/teleop_twist_keyboard.git

Source the underlay (noetic)

    source /opt/ros/noetic/setup.bash

### 2.2 Compile the workspace

    cd ~/ros/workspaces/ros_crash_ws/
    catkin build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

### 2.3 Run the binary teleop_twist_keyboard.py and play with it

You will need three terminals for this task.

#### Terminal 1

    source /opt/ros/noetic/setup.bash
    roscore

You should keep this terminal open for the rest of the course. This will keep the roscore running in a independent terminal, which is a good idea!

#### Terminal 2

    cd ~/ros/workspaces/ros_crash_ws/
    source devel/setup.bash
    rosrun teleop_twist_keyboard teleop_twist_keyboard.py

#### Terminal 3

    cd ~/ros/workspaces/ros_crash_ws/
    source devel/setup.bash
    rostopic echo /cmd_vel

You can use the keyboard to publish twist commands. Read the instructions in **Terminal 2**.

### 2.4 Modify the node teleop_twist_keyboard.py

a) Add a customized print message in the file

    ~/ros/workspaces/ros_crash_ws/src/teleop_twist_keyboard/teleop_twist_keyboard.py 

b) Compile your workspace

c) Open a new terminal (CTRL+ALT+t) and run

    source /opt/ros/noetic/setup.bash
    rosrun teleop_twist_keyboard teleop_twist_keyboard

**Q1**: What happened? Can you see your custom message? Hint: underlay vs overlay

### 2.5 Modify the binary teleop_twist_keyboard.py to generate the following motions

  Linear motions:

    u: left-front
    i: front
    o: right-front
    j: left
    k: stop
    l: right
    m: left-back
    ,: back
    .: right-back

    Angular Motions:
    w: counter-clockwise
    e: stop
    r: clock-wise

  Speed:

    + increase linear speed +0.1
    - decrease linear speed -0.1
    * increase angular speed +0.1
    / decrease angular speed -0.1

### 2.6 Compile your workspace

In a new terminal

    cd ~/ros/workspaces/ros_crash_ws/
    source /opt/ros/noetic/setup.bash
    catkin build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

### 2.7 Test your new node

Open another terminal and source your workspace (overlay)

    cd ~/ros/workspaces/ros_crash_ws/
    source devel/setup.bash
    rosrun teleop_twist_keyboard teleop_twist_keyboard cmd_vel:=turtle_twist

**Q2**: Does it work? Can you see your changes? How can you verify it? Hint: use rostopic

After running rostopic with the correct arguments, you should see something like this:

    linear: 
      x: 0.0
      y: 0.0
      z: 0.0
    angular: 
      x: 0.0
      y: 0.0
      z: 0.0
    ----

---

## Task 3 Create a new ros cpp package turtle msgs

[back](#table-of-contents)

### 3.1 Use the ros create package function

    cd ~/ros/workspaces/ros_crash_ws/src
    catkin_create_pkg turtle_msgs std_msgs geometry_msgs rospy roscpp message_generation message_runtime

A filesystem wil be created with the correct structure for a ros c++ package:

```bash
.
└── ros_crash_ws
    ├── build
    ├── CMakeLists.txt 
    ├── devel
    ├── logs
    └── src
        ├── teleop_twist_keyboard
        │   ├── CHANGELOG.rst
        │   ├── CMakeLists.txt
        │   ├── package.xml
        │   ├── README.md
        │   └── teleop_twist_keyboard.py
        └── turtle_msgs
            ├── CMakeLists.txt
            ├── include
            ├── package.xml
            └── src
```

**turtle_msgs**: is a folder containing our ros package

**CMakeLists.txt**: CMake configuration file. Here we specify the dependencies of this packages, which files this package will provide, and much more.

**include/turtle_msgs**: This folder contains the c++ headers provided by this package.

**package.xml**: Provides general information of this package. This files is also used by other ros package to define dependencies.

**src**: This folder will contain the source files of the binaries that this ros package provides. The binaries can have the form of executables (Nodes) or libraries that can be used by other ros packages.

Since this is an interfaces ros package (messages and services), the **src** and **include** folders will be empty.
We will create new folders **msg** and **srv** to contain the message and service definitions.

### 3.2 Compile the new package

In the terminal where you have sourced the underlay (noetic), compile your workspace. If you closed it, open a
new terminal move to your workspace folder and source the underlay (noetic).

    catkin build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

**Q3**: Any problems?

### 3.3 Create a custom turtle message (TurtleStateStamped)

a) Create a ~/ros/workspaces/ros_crash_ws/src/turtle_msgs/msg folder with a file named
TurtleStateStamped.msg

b) Modify the CMakeLists.txt and package.xml to include the new message. Hint: see the reference files and
modify them accordingly

c) Compile the workspace. Hint: overlay vs underlay

### 3.4 Verify that your new message has been created

Open a terminal and source your workspace (overlay).

You nee to run **rosmsg** with the correct arguments. Then, you should see something similar to this:

```bash
    std_msgs/Header header
      uint32 seq
      time stamp
      string frame_id
    geometry_msgs/Pose2D pose
      float64 x
      float64 y
      float64 theta
    geometry_msgs/Twist vel
      geometry_msgs/Vector3 linear
        float64 x
        float64 y
        float64 z
      geometry_msgs/Vector3 angular
        float64 x
        float64 y
        float64 z
```

**Q4**: Can you find your new message? Hint: overlay vs underlay

---

## Task 4 Create a new ros cpp package turtle ctrl

[back](#table-of-contents)

This ros package will contain the visualization and control modules (C++).

This new package will use the previously created package turtle_msgs. In other words, turtle_msgs is a dependency
for this new package.

### 4.1 Use the ros create package function

    cd ~/ros/workspaces/ros_crash_ws/src
    catkin_create_pkg turtle_ctrl std_msgs geometry_msgs turtle_msgs tf2 tf2_ros rospy roscpp

### 4.2 Compile the new package

Don't forget to compile in the terminal where you sourced ros noetic (underlay)

You can compile the complete workspace:

    catkin build --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

or just the new package (and its dependencies)

    catkin build turtle_ctrl --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

### 4.3 Create the TurtleVis Class

The turtleVis class will provide a library with the functions needed to deploy the visualization module. The input is a continuous topic (**TurtleStateStamped**) with the current turtle's position. The output is a visualization marker and the corresponding **tf** frame.

![TurtleVisModule](/Resources/Figures/Modules/turtleVis.png "Turtle Visualization Module")

Copy the TurtleVis class files TurtleVis.h and TurtleVis.cpp to their corresponding folders

### 4.4 Modify the CMakeFile

Modify the CMakeFile to create a shared library with the TurtleVis class

### 4.5 Copy the turtle visualization node

Copy the application file turtle_vis_node.cpp

### 4.6 Add the new node into the CMakeFile

Modify the CMakeFile to create a new binary (Exec)

### 4.7 Compile your workspace (or single ros package)

Hint: underlay or overlay?

### 4.8 Copy the configuration and parameters files

a) Copy the file "turtle.rviz" into turtle_ctr/rviz/turtle.rviz

b) Copy the files "turtle_ctrl.yaml" and "turtle_vis.yaml" into the folder turtle_ctr/configs/

### 4.9 Test your application

You will need three terminals. You need to source your workspace (overlay) in each of them.

#### Terminal 1: Turtle Visualization node

    rosparam load src/turtle_ctrl/configs/turtle_vis.yaml
    rosrun turtle_ctrl turtle_vis_node

If you get the error:

    ERROR: Unable to communicate with master!   

It means that you either closed the terminal running **roscore**, or you stopped **roscore**. Open a terminal and run again the roscore. Keep roscore running!

#### Terminal 2: Rviz

    rosrun rviz rviz -d src/turtle_ctrl/rviz/turtleTutorial.rviz
  
#### Terminal 3: Publish a TurtleState topic

    rostopic pub --rate 1 /turtle_pose turtle_msgs/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

You should see something similar to this:

![TurtleVis](/Resources/Figures/TurtleVis/01_turtle.png "Turtle Visualization")

**Q5**: What can you see? Hint: Change the fixed frame from "map" to "world" in rviz

### 4.10 Change the position and orientation of the turtle with a new message

Hint: re-use the rostopic command with different values

---

## Task 5 Let us create a lunch file to automate the above process

[back](#table-of-contents)

### 5.1 Copy the launch file

Copy the file "turtle_vis_test_launch.py" into the folder turtle_ctrl/launch/

### 5.2 Run the new launch file

Stop the running nodes (process) in all the terminals (CTRL+c).

#### Terminal 1: Launch file

    roslaunch turtle_ctrl TurtleVis.launch

#### Terminal 2: Publish a TurtleState topic

    rostopic pub --rate 1 /turtle_pose turtle_msgs/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

You should get the same output as before:

![TurtleVis](/Resources/Figures/TurtleVis/01_turtle.png "Turtle Visualization")

**Q6**: Does it work? What happened?

---

## Task 6 Create the controller

[back](#table-of-contents)

The control module should receive the target pose of the turtle (Continuous or Discrete), and it should generate a continuous topic with the turtle's position. Then, we have two different threads. One is triggered by the target pose topic, and the other computes the new turtle's position and publishes it. This module should also publish a **tf** frame for the target position.

![TurtleCtrlModule](/Resources/Figures/Modules/turtleCtrl.png "Turtle Control Module")

### 6.1 Copy the TurtleCtrl class

Copy the class files TurtleCtrl.cpp and TurtleCtrl.h to the corresponding folders

### 6.2 Modify the CMakeFile

Modify the CMakeFile to create a shared library with the control class

### 6.3 Copy the turtle control node

Copy the application file turtle_ctrl_node.cpp

### 6.4 Add the new node into the CMakeFile

Modify the CMakeFile to create a new binary (Exec)

### 6.5 Compile your workspace (or single ros package)

Hint: underlay?

### 6.6 Test your application

a) Copy the launch file TurtleVis.launch and renamed as TurtleTest.launch

b) Modify the new launch file to include the control node and load the turtle_ctrl.yaml parameter file

c) Run the new launch file

**Q7**: How can you test your application? Hint: rostopic

ros topic pub --rate 1 /turtle_cmd turtle_msgs/msg/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

---

## Task 7 Trajectory generator

[back](#table-of-contents)

We will implement two methods to generate the commanded turtle pose for the controller.

First, transform the Twist (turtle velocity) message generated from the node teleop_twist_keyboard to continuous commanded turtle poses.

Second, we will create a node that provides a service to request a desired turtle pose. This node will use the requested turtle pose and generate a continuous smooth trajectory for the commanded turtle pose.

The output of both nodes is a TurtleStateStamped message (/turtle_cmd), which is connected to the controller to change the pose of the turtle.

### 7.1 Create a python ros package

Create a new ros python package "turtle_trajectory_generator"

    cd ~/ros/workspaces/ros_crash_ws/src
    catkin_create_pkg turtle_trajectory_generator turtle_msgs rospy

A filesystem wil be created with the correct structure for a ros python package:

```bash
.
└── ros_crash_ws
    └── src
        ├── teleop_twist_keyboard
        │   ├── CHANGELOG.rst
        │   ├── CMakeLists.txt
        │   ├── package.xml
        │   ├── README.md
        │   └── teleop_twist_keyboard.py
        ├── turtle_ctrl
        │   ├── CMakeLists.txt
        │   ├── configs
        │   ├── include
        │   ├── launch
        │   ├── package.xml
        │   ├── rviz
        │   ├── src
        │   └── urdf
        ├── turtle_msgs
        │   ├── CMakeLists.txt
        │   ├── include
        │   ├── msg
        │   ├── package.xml
        │   ├── src
        │   └── srv
        └── turtle_trajectory_generator
            ├── CMakeLists.txt
            ├── package.xml
            └── src
```

The contents of the folders are similar to the turtle_msgs package. We will modify the CMakeList to indicate the python scripts and modules.

### 7.2 Set up your new ros package

WWe need to modify the CMakeFile and package.xml files. This ros package is a rospy package. Then, we need to provide information on where to install the python scripts and modules used by rosrun, roslaunch, and other ros packages. Also, you can add new messages or services if needed. In our case, we will define a separated ros package with all the msgs and services needed in this project (turtle_msgs).

a) Create filesystem for the python ros package

    cd /home/usr/ros/workspaces/ros_crash_ws/src/turtle_trajectory_generator
    mkdir scripts
    mkdir src/turtle_trajectory_generator
    touch src/turtle_trajectory_generator/__init__.py

**scripts** is a folder containing the ros python nodes (scripts)

**src/turtle_trajectory_generator** is a folder containing our python modules. As a rule, this folder should have the **same name** as the ros package. These modules are python libraries that can be used in this ros package and shared with other ros packages.

NOTE: Each subfolder containing python scripts or modules must be indicated with a __init__.py empty file!

### 7.3 Add python scripts

Copy the modified file teleop_twist_keyboard.py from the ros package teleop_twist_keyboard to this new package.

### 7.4 Configure your python ros package

You need to include the following macros in the CMakeLists with the information of the target binaries. This will install the python scripts and modules and they can be used by other packages.

    catkin_python_setup()
    catkin_install_python

### 7.5 Configure your python ros package

Copy the setup.py file in the root folder turtle_trajectory_generator/

### 7.6 Compile the workspace

catkin build turtle_trajectory_generator --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

**NOTE**: If you are using VSCode with the Catkin-tools extension, you can compile the workspace with CTRL+Shift+b, and selecting the option:

    catkin_build: build 

### 7.7 Test your python script

a) First, remove the folder **~/ros/workspaces/ros_crash_ws/src/teleop_twist_keyboard**. We don't need it since we have ported the python script teleop_twist_keyboard.py to our new ros python package.

b) Run the script using the ros package turtle_trajectory_generator.

    rosrun turtle_trajectory_generator teleop_twist_keyboard.py

**Q8**: How can you verify the new python script? Hint: ros topic

**Q9**: Can you run the new ros python script? Hint: underlay, overlay, re-source

### 7.8 Create a desired pose service

We need to create a new service to set the desired turtle pose. We are using the ros package turtle_msgs as the main package to contain all the messages and service of this project.

a) Create the folder turtle_msgs/srv

b) Copy file SetDesiredPose.srv into the new folder turtle_msgs/srv

c) Modify the CMakeFile to include the new service. You need to add the lines:

    add_service_files(
    FILES
    SetDesiredPose.srv
    )

d) Compile the ros package turtle_msgs

**Q10**: How can you verify that the new service is properly defined? Hint: rossrv show  

### 7.9 Unresolved python symbols

You may get an error message informing that some modules cannot be resolved. You need to include the paths of the ros packages in python configuration file.

a) Configure .vscode/setting.json python.autoComplete.extraPaths and python.analysis.extraPaths by adding:

    "python.autoComplete.extraPaths": [
        "${workspaceFolder}/src/turtle_trajectory_generator",
        "${workspaceFolder}/src/turtle_trajectory_generator/src",
        "${workspaceFolder}/devel/lib/python3/site-packages",
        "/opt/ros/noetic/lib/python3/site-packages",
        "/usr/lib/python3/dist-packages/"
    ],
    "python.analysis.extraPaths": [
        "${workspaceFolder}/src/turtle_trajectory_generator",
        "${workspaceFolder}/src/turtle_trajectory_generator/src",
        "${workspaceFolder}/devel/lib/python3/site-packages",
        "/opt/ros/noetic/lib/python3/site-packages",
        "/usr/lib/python3/dist-packages/"
    ],

### 7.10 Add Python modules

a) Create the trajectory generator python module. Use the file trajectory_generator_module.py and copy it to the correspondent python modules folder turtle_trajectory_generator/src/turtle_trajectory_generator

b) Configure your python package and include the new modules. You need to modify the setup.py file.

c) Compile the workspace again.

### 7.11 Add python script Twist2CMD

We need to develop a module that receives Twist velocity message and transforms it into commanded TurtleStateStamped message.

![Twist2CMDModule](/Resources/Figures/Modules/twist2cmd.png "Turtle Twist2CMD Module")

We need to include some python scripts that will use the classes defined in the python module (library).

a) Copy the file twist2cmd.py to the scripts folder turtle_trajectory_generator/turtle_trajectory_generator/scripts

This script converts a Twist message to commanded turtle pose message (TurtleStateStamped) needed as reference in the controller.

b) Add the script in the CMakeFile install macro

c) Compile your ros package

### 7.12 Test your new script

The ros python script uses parameters that are described in a yaml file and should be loaded to the ros parameter server.

a) Create a folder turtle_trajectory_generator/configs

b) Copy the parameters file turtle_traj_gen.yaml file into the new folder

#### Terminal 1: Keyboard node

    rosrun turtle_trajectory_generator teleop_twist_keyboard.py cmd_vel:=turtle_twist

#### Terminal 2: Twist2cmd

    rosparam load src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml
    turtle_trajectory_generator twist2cmd.py

**Q11**: How can you verify your script? Hint: ros topic, rqt_plot

If you use rqt_plot, you should see something similar to this:

![TurtleVis](/Resources/Figures/Keyboard/02_plot.png "Plot with the turtle motions")

### 7.13 Add python script Polynomial Trajectory

We need a node that provides a service where the user can request a desired turtle pose. The python script with the node will use this desired pose and generate a smooth trajectory from the current turtle pose to the desired one.

![PolyTrajectory](/Resources/Figures/Modules/polyTrajectory.png "Turtle Polynomial Trajectory Module")

a) Copy the file poly_trajectories.py into the scripts folders

b) Add the script in the CMakeFile install macro

c) Compile your ros package

### 7.14 Test your new script

#### Terminal 1: Polynomial trajectory generation

    rosparam load src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml
    rosrun turtle_trajectory_generator poly_trajectories.py

**Q12**: Can you find the description of the new service? Hint: rosservice

After running the command rosservice with the correct arguments in a terminal, you should see:

    Node: /listener_92344_1646485591673
    URI: rosrpc://dean-Precision-5540:37035
    Type: turtle_msgs/SetDesiredPose
    Args: turtle_d time

Hint: Did you source your overlay (workspace)?

#### Terminal 2: Call the service

    rosservice list

Verify that the service is already advertised

    rosservice call /set_desired_pose "turtle_d:
    x: 1.0
    y: 1.0
    theta: 2.0
    time: 5.0"

**Q13**: How can you test the trajectory generator nodes only? Hint: ros run rqt_plot rqt_plot
**Q14**: Does it work? Hint: did you configure your CMakeFile file?

Again, using rqt_plot, you should see something like this:

![TurtleVis](/Resources/Figures/Poly/02_plot.png "Plot with the turtle motions")

---

## Task 8 Evaluate full system

[back](#table-of-contents)

We need to test the two versions of the system. One where we can control the turtle with the keyboard and the other where we can control the turtle by defining the desired turtle's position.

### 8.1 Turtle Control and visualization

For both test, we need the turtle control node and the visualization (rviz).

#### Terminal 1: Launch file with visualization and control

    roslaunch turtle_ctrl TurtleTest.launch

### 8.1 Keyboard control demo

#### Terminal 2: keyboard

    rosrun turtle_trajectory_generator teleop_twist_keyboard.py cmd_vel:=turtle_twist

#### Terminal 3: twist2cmd and ros parameters

    rosparam load src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml
    rosrun turtle_trajectory_generator twist2cmd.py

**Q15**: Can you see the turtle moving with the keyboard commands? Hint: Verify all the topics and services. Are all of them correctly configured?

You should see the turtle moving whit the keyboard commands:

![TurtleVis](/Resources/Figures/Keyboard/01_turtle.png "Turtle Visualization")

![TurtleVis](/Resources/Figures/Keyboard/02_plot.png "Plot with the turtle motions")

### 8.2 Polynomial Trajectory

#### Terminal 2: polynomial trajectory generator node

    rosparam load src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml
    rosrun turtle_trajectory_generator poly_trajectories.py

#### Terminal 3: Calling the service

    rosservice list
    rosservice call /set_desired_pose "turtle_d:
      x: 1.0
      y: 1.0
      theta: 2.0
    time: 5.0"

**Q16**: Can you see the turtle moving with the keyboard commands? Hint: Verify all the topics and services. Are all of them correctly configured?

You should see the turtle moving smoothly from the initial position to the target position in the given time:

![TurtleVis](/Resources/Figures/Poly/01_turtle.png "Turtle Visualization")

![TurtleVis](/Resources/Figures/Poly/02_plot.png "Plot with the turtle motions")

---

## Task 9 Record data and play it bagfiles

[back](#table-of-contents)

In this task, we will move the turtle using the keyboard commands and record the commanded turtle's trajectory. Then,
we will replay the trajectory.

### 9.1 Recording data

First, we will record the produced data into bag files. For this, you will need five terminals.

#### Terminal 1: Launch files

    roslaunch turtle_ctrl TurtleTest.launch

#### Terminal 2: twist2cmd and ros parameters

    rosparam load src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml
    rosrun turtle_trajectory_generator twist2cmd.py

#### Terminal 3: keyboard

    rosrun turtle_trajectory_generator teleop_twist_keyboard.py cmd_vel:=turtle_twist

#### Terminal 4: RQT_PLOT  

    ros run rqt_plot rqt_plot

#### Terminal 5: Bagfile recording

First, we need to identify which data (topic) we would like to record. This can be done by analyzing the node
communication graph.

    ros run rqt_graph rqt_graph

We identify the topic **/turtle_cmd** as the target one. Then, we can record that topic:

    cd ~/ros/workspaces/ros_crash_ws/
    mkdir bags/
    cd bags/
    ros bag record -o turtle_cmd /turtle_cmd

Now, move the turtle with the keyboard in Terminal 3. Record a few seconds and stop the nodes in the terminals T2, T3,
and T5 (twist2cmd, keyboard, and bag, respectively).

You will find a new file named **turtle_cmd_*.bag** that contains the recorded data.

You can get some information of the bag file using the command:

    rosbag info turtle_cmd_*.bag

The output should be something like this:

    path:        turtle_cmd_bag_2022-03-05-14-17-37.bag
    version:     2.0
    duration:    15.5s
    start:       Mar 05 2022 14:17:37.36 (1646486257.36)
    end:         Mar 05 2022 14:17:52.88 (1646486272.88)
    size:        240.2 KB
    messages:    1553
    compression: none [1/1 chunks]
    types:       turtle_msgs/TurtleStateStamped [95514d8a53e4dd38f2f3f8a4ae9b54f6]
    topics:      /turtle_cmd   1553 msgs    : turtle_msgs/TurtleStateStamped

### 9.2 Playback the recorded data

Now,we will play the topic /turtle_cmd and see the motion in Rviz.

Before playing the bag file, we need to stop some nodes!

**Q17**: Which nodes should we stop? Why?

#### Terminal 5: Replay bagfile

    ros bag play turtle_cmd_*.bag

The last command will replay the topic /turtle_cmd one time. If we need to replay in a loop, we can use a special flag:

    ros bag play -l turtle_cmd_*.bag -l

**Q18**: Can you see the turtle moving?
